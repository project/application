<?php
/**
 * @file
 * Application entity class declaration.
 */

/**
 * Application class.
 */
class Application extends Entity {
  /**
   * Overrides Entity::defaultLabel().
   */
  protected function defaultLabel() {
    return $this->title;
  }

  /**
   * Overrides Entity::defaultURI().
   */
  protected function defaultUri() {
    return array('path' => 'application/' . $this->identifier());
  }
}
