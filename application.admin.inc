<?php
/**
 * @file
 * Administration forms for the Postings & Applications module.
 */

/**
 * Generates the posting editing form.
 */
function application_posting_form($form, &$form_state, $posting, $op = 'edit') {
  global $user;
  $form_state['application_posting'] = $posting;

  if ($op == 'clone') {
    $posting->name .= ' (cloned)';
    $posting->type .= '';
  }

  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => $posting->name,
    '#description' => t('The human-readable title of this posting.'),
    '#required' => TRUE,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($posting->type) ? $posting->type : '',
    '#maxlength' => 32,
    '#description' => t('The machine-readable name of this posting, containing only letters, numbers and underscores.'),
    '#machine_name' => array(
      'exists' => 'application_posting_load',
      'source' => array('name'),
    ),
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'text_format',
    '#format' => isset($posting->description['format']) ? $posting->description['format'] : filter_default_format($user),
    '#default_value' => isset($posting->description['value']) ? $posting->description['value'] : '',
    '#description' => t('Any relevant information about the posting.'),
    '#required' => FALSE,
  );

  $form['applimit'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($posting->applimit) ? $posting->applimit : '',
    '#description' => 'The maximum number of applications each user may submit (0 for unlimited).',
    '#required' => FALSE,
  );

  // Format the posting deadline accordingly.
  $timezone = isset($user->timezone) ? $user->timezone : variable_get('date_default_timezone', 'UTC');
  if (isset($posting->deadline)) {
    // @TODO: Refactor to helper function for tests, etc. to call on.
    $deadline_parts = explode('/', format_date($posting->deadline, 'custom', 'n/j/Y', $timezone));
    $posting_deadline = array(
      'month' => $deadline_parts[0],
      'day' => $deadline_parts[1],
      'year' => $deadline_parts[2],
    );
  }

  $form['deadline'] = array(
    '#type' => 'date',
    '#default_value' => isset($posting_deadline) ? $posting_deadline : '',
    '#description' => 'The date after which no applications will be accepted.',
    '#required' => FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );

  if (!$posting->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('application_posting_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Validate handler for creating/editing postings.
 */
function application_posting_form_validate(&$form, &$form_state) {
  $type = $form_state['values']['type'];

  // Convert date to timestamp for saving.
  $m = $form_state['values']['deadline']['month'];
  $d = $form_state['values']['deadline']['day'];
  $y = $form_state['values']['deadline']['year'];

  // Convert to timestamp before saving.
  $timezone = isset($user->timezone) ? $user->timezone : variable_get('date_default_timezone', 'UTC');
  $deadline = strtotime($m . '/' . $d . '/' . $y);
  if ($deadline === FALSE) {
    form_set_error('deadline', 'Please provide a valid date.');
  }
  else {
    $deadline = format_date($deadline, 'custom', 'U', $timezone);
    $form_state['values']['deadline'] = $deadline;
  }

  if (!empty($form_state['values']['description']['format'])) {
    $format = $form_state['values']['description']['format'];
    if (!filter_access(filter_format_load($format))) {
      form_set_error('description][format', 'You do not have permission to use this text format.');
    }
  }

  if (!ctype_digit($form_state['values']['applimit'])) {
    form_set_error('applimit', 'Please provide a valid application limit.');
  }
}

/**
 * Submit handler for creating/editing postings.
 */
function application_posting_form_submit(&$form, &$form_state) {
  $posting = entity_ui_form_submit_build_entity($form, $form_state);
  application_posting_save($posting);

  // Redirect back to posting list.
  $form_state['redirect'] = 'admin/structure/postings';
}

/**
 * Submit handler for opting to delete postings.
 */
function application_posting_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/posting/' . $form_state['application_posting']->type . '/delete';
}

/**
 * Posting deletion confirmation form.
 */
function application_posting_form_delete_confirm($form, &$form_state, $posting) {
  $form_state['application_posting'] = $posting;
  $form['psid'] = array(
    '#type' => 'value',
    '#value' => entity_id('application_posting', $posting),
  );
  return confirm_form($form,
    t('Are you sure you want to delete the posting %name?',
      array('%name' => entity_label('application_posting', $posting))),
    t('This action cannot be undone'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Posting delete form submit handler.
 */
function application_posting_form_delete_confirm_submit($form, &$form_state) {
  $posting = $form_state['application_posting'];
  application_posting_delete($posting);

  watchdog('application_posting',
    '@type: deleted %name.',
    array('@type' => $posting->type, '%title' => $posting->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $posting->type, '%title' => $posting->label)));

  $form_state['redirect'] = 'admin/structure/postings';
}

/**
 * Add new application page callback.
 */
function application_add($type) {
  $posting = application_posting_load($type);

  $application = entity_create('application', array('type' => $type));
  drupal_set_title(t('Apply to @name', array('@name' => entity_label('application_posting', $posting))));

  $output = drupal_get_form('application_form', $application);

  return $output;
}

/**
 * Application form.
 */
function application_form($form, &$form_state, $application) {
  $form_state['application'] = $application;

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $application->title,
    '#required' => TRUE,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $application->uid,
  );

  field_attach_form('application', $application, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array('#weight' => 100);

  // Move form-level submit handlers to the proper form element.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('application_form_submit'),
  );

  $aid = entity_id('application', $application);
  if (!empty($aid) && application_access('edit', $application)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('application_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'application_form_validate';

  return $form;
}

/**
 * Application form validate handler.
 */
function application_form_validate($form, &$form_state) {
  $type = $form_state['application']->type;

  $post_deadline = db_query('SELECT deadline FROM {application_posting} 
    WHERE type = :type', array('type' => $type))->fetchField();
  if (REQUEST_TIME > $post_deadline + 86399) {
    form_set_error('deadline', 'Applications for this posting are no longer being accepted.');
  }

  // If applimit > 0, check number of apps, and throw error if limit reached.
  $limit = db_query('SELECT applimit FROM {application_posting}
    WHERE type = :type', array('type' => $type))->fetchField();

  if ($limit > 0) {
    // Count the number of applications submitted by this user.
    global $user;

    $total_apps = db_query("SELECT COUNT('aid') AS count FROM {application} a
      INNER JOIN {application_posting} ap ON a.type = ap.type
      WHERE a.type = :type AND a.uid = :uid",
      array('type' => $type, 'uid' => $user->uid))->fetchField();

    if ($limit >= $total_apps) {
      $app_text = ($total_apps > 1) ? 'applications' : 'application';
      form_set_error(NULL, 'Only $limit $app_text may be submitted per user.');
    }
  }
}

/**
 * Application submit handler.
 */
function application_form_submit($form, &$form_state) {
  $application = $form_state['application'];
  entity_form_submit_build_entity('application', $application, $form, $form_state);
  application_save($application);

  $app_uri = entity_uri('application', $application);
  $form_state['redirect'] = $app_uri['path'];

  drupal_set_message(t('Application %title saved.', array('%title' => entity_label('application', $application))));
}

/**
 * Application deletion submit handler.
 */
function application_form_submit_delete($form, &$form_state) {
  $application = $form_state['application'];
  $app_uri = entity_uri('application', $application);
  $form_state['redirect'] = $app_uri['path'] . '/delete';
}

/**
 * Application deletion confirmation form.
 */
function application_delete_form($form, &$form_state, $application) {
  $form_state['application'] = $application;
  $form['aid'] = array(
    '#type' => 'value',
    '#value' => entity_id('application', $application),
  );
  $app_uri = entity_uri('application', $application);
  return confirm_form($form,
    t('Are you sure you want to delete the application %title?', array('%title' => entity_label('application', $application))),
    $app_uri['path'],
    t('This action cannot be undone.'),
    t('Confirm'),
    t('Cancel')
  );
}

/**
 * Delete application confirmation form submit handler.
 */
function application_delete_form_submit($form, &$form_state) {
  $application = $form_state['application'];
  application_delete($application);

  drupal_set_message(t('Deleted application %title.', array('%title' => entity_label('application', $application))));

  $form_state['redirect'] = '<front>';
}


/**
 * Page callback to view all applications submitted to a posting.
 */
function application_posting_view_applications($posting) {
  global $user;

  drupal_set_title('Applications for ' . entity_label('application_posting', $posting));

  // Get entity IDs to load via EntityFieldQuery.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'application')
    ->propertyCondition('type', $posting->type);
  $result = $query->execute();

  $apps = array();
  if (!empty($result['application'])) {
    $ids = array_keys($result['application']);
    $apps = application_load_multiple($ids);
    $timezone = isset($user->timezone) ? $user->timezone : variable_get('date_default_timezone', 'UTC');
  }

  $table = array();
  $table['header'] = array(
    'title' => array(
      'data' => t('Title'),
      'field' => 'application.title',
    ),
    'author' => array(
      'data' => t('Author'),
    ),
    'created' => array(
      'data' => t('Title'),
      'field' => 'application.created',
    ),
    'updated' => array(
      'data' => t('Last Modified'),
      'field' => 'application.updated',
      'sort' => 'desc',
    ),
    'ops' => array(
      'data' => t('Operations'),
      'colspan' => 3,
    ),
  );
  $table['rows'] = array();

  foreach ($apps as $app) {
    $wrapper = entity_metadata_wrapper('application', $app);
    $table['rows'][] = array(
      'title' => array(
        'data' => l($app->title, 'application/' . $app->aid),
      ),
      'author' => array(
        'data' => $wrapper->uid->name->value(),
      ),
      'created' => array(
        'data' => format_date($app->created, 'short', '', $timezone),
      ),
      'updated' => array(
        'data' => format_date($app->updated, 'short', '', $timezone),
        'sort' => 'desc',
      ),
      'edit' => array(
        'data' => l(t('Edit'), 'application/' . $app->aid . '/edit'),
      ),
      'delete' => array(
        'data' => l(t('Delete'), 'application/' . $app->aid . '/delete'),
      ),
    );
  }

  return (!empty($apps)) ? theme('table', $table) : t('No applications have been submitted for this posting.');
}
