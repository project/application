<?php
/**
 * @file
 * Application entity controller class declaration.
 */

/**
 * Application entity controller.
 */
class ApplicationController extends EntityAPIController {
  /**
   * Overrides EntityAPIController::create().
   */
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'title' => '',
      'created' => REQUEST_TIME,
      'updated' => REQUEST_TIME,
      'uid' => $user->uid,
    );

    return parent::create($values);
  }

  /**
   * Overrides EntityAPIController::save().
   */
  public function save($entity) {
    $entity->updated = REQUEST_TIME;
    return parent::save($entity);
  }

  /**
   * Overrides EntityAPIController::buildContent().
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('application', $entity);
    $content['author'] = array('#markup' => '<div class="field application-applicant"><div class="field-label">' . t('Applicant: ') . '</div><div class="field-items">' . l($wrapper->uid->name->value(array('sanitize' => TRUE)), 'user/'.$wrapper->uid->raw()) . '</div></div>', '#weight' => -50);

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
