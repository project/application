<?php
/**
 * @file
 * Posting entity class declaration.
 */

/**
 * Posting class. Used as bundles for applications.
 */
class ApplicationPosting extends Entity {
  public $type;
  public $label;
  public $weight = 0;

  /**
   * Overrides Entity::__construct().
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'application_posting');
  }

  /**
   * Overrides Entity::defaultLabel().
   */
  protected function defaultLabel() {
    return $this->title;
  }

  /**
   * Overrides Entity::defaultUri().
   */
  protected function defaultUri() {
    return array('path' => 'postings/' . $this->identifier());
  }

  /**
   * Determine whether a posting is locked for deletion.
   * 
   * Postings provided in code and fixed postings are always treated as locked.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
