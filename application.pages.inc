<?php
/**
 * @file
 * Page callbacks for Postings & Applications module.
 */

/**
 * Application view callback.
 */
function application_view($application) {
  drupal_set_title(entity_label('application', $application));
  return entity_view('application', array(entity_id('application', $application) => $application), 'full');
}

/**
 * Application posting view callback.
 */
function application_posting_view($posting) {
  drupal_set_title(entity_label('application_posting', $posting));
  return entity_view('application_posting', array(entity_id('application_posting', $posting) => $posting), 'full');
}

/**
 * Page to view all current postings to add new application.
 */
function application_posting_view_all() {
  drupal_set_title('Current Postings');

  // Get entity IDs to load via EntityFieldQuery.
  $query = new EntityFieldQuery();
  // @TODO: Change end-of-day adjustment to happen in the database.
  $query->entityCondition('entity_type', 'application_posting')
    ->propertyCondition('deadline', time() - 86399, '>');
  $result = $query->execute();

  return (!empty($result)) ? entity_view('application_posting', application_posting_load_multiple(array_keys($result['application_posting'])), 'teaser') : t('There are no current postings available.');
}

/**
 * Page to view all applications for a user.
 */
function application_view_user_list($account = NULL) {
  global $user;

  if (!isset($user)) {
    $account = $user;
  }

  drupal_set_title('My Applications');

  // Get entity IDs to load via EntityFieldQuery.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'application')
    ->propertyCondition('uid', $account->uid);
  $result = $query->execute();

  return (!empty($result)) ? entity_view('application', application_load_multiple(array_keys($result['application'])), 'teaser') : t('You have not yet submitted any applications.');
}
