<?php
/**
 * @file
 * Posting entity controller class declaration.
 */

/**
 * Posting entity controller.
 */
class ApplicationPostingController extends EntityAPIControllerExportable {
  /**
   * Overrides EntityAPIControllerExportable::create().
   */
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'name' => '',
      'description' => '',
      'applimit' => 0,
      'deadline' => NULL,
    );

    return parent::create($values);
  }

  /**
   * Overrides EntityAPIController::save().
   *
   * A menu rebuild must be triggered after a posting is saved.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    variable_set('menu_rebuild_needed', TRUE);
    return parent::save($entity, $transaction);
  }
}
