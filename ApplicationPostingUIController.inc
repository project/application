<?php

/**
 * @file
 * UI Controller declaration for application postings.
 */

/**
 * UI controller class declaration for postings.
 */
class ApplicationPostingUIController extends EntityDefaultUIController {
  /**
   * Overrides EntityDefaultUIController::hook_menu().
   */
  public function hook_menu() {
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%entity_object';

    $items = parent::hook_menu();
    $items[$this->path]['access arguments'] = array('administer', $this->entityType);
    $items[$this->path]['description'] = 'Manage postings.';
    $items[$this->path . '/manage/%application_posting/applications'] = array(
      'title' => 'Applications',
      'page callback' => 'application_posting_view_applications',
      'page arguments' => array($id_count + 1),
      'access callback' => 'application_access',
      'access arguments' => array('view', 'application_posting'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 50,
      'file' => 'application.admin.inc',
      'file path' => drupal_get_path('module', 'application'),
    );
    return $items;
  }
}
