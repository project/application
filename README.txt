CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Design Decisions
 
 
INTRODUCTION
------------

Applications and Postings allows users to submit postings to the site; these 
could be job postings, requests for proposals, calls for submissions, and so
forth. Each posting has its own fieldable application form (much in the same way
each content type has its own set of fields), allowing administrators to create
separate application forms for each posting. Other users may then submit
applications against these postings, up until a predetermined (and automatically
enforced) deadline. These applications may be reviewed later on, potentially
integrating with other modules for more advanced workflows.


INSTALLATION
------------

Follow the instructions provided at:
http://drupal.org/documentation/install/modules-themes/modules-7


DESIGN DECISIONS
----------------

Q:  Why not use nodes to represent content instead?
A:  The same reason modules like Commerce don't shoehorn everything into nodes:
    it isn't "just content." Early on in the search for a robust solution to
    this problem set, a number of things became evident:

      * Each application must know which posting it is in response to.
      * Each posting must support its own distinct application form. In an ideal
        world, this form should be fieldable.
      * Application creation must be governed by consistent behavior, such as
        deadlines and per-user limits, set on a per-posting basis.

    Creating a relationship between postings and applications meant using a
    module like Relation or Entity Reference to establish the link between 
    individual nodes, presumably of separate content types. Such a relationship
    would either have to be maintained by the user (messy and poor UX), or
    behind the scenes (error prone and kludgy). 
    
    Further, if these nodes were of separate content types, providing different
    fields for each application form meant having a content type for each, 
    which quickly becomes untenable as postings grow in size.
    
    Finally, these postings and applications all have consistent, interwoven
    behavior. Doing this with nodes, while possible, means a lot of conditional
    hackery with modules like Rules and Node Limit. This configuraiton must also
    be updated with each new application-related content type created. All of
    these concerns taken together suggested nodes may not be the way to go.
    
    For very simple cases that aren't expected to become more complex as time
    goes on, users are encouraged to use the right tool for the job. It's
    possible to create a very simple posting and application site using core,
    and if you can, you should at least consider it.

Q:  Why entities, specifically?
A:  Not only are entities forward-compatible with solid integration with other
    modules and APIs, they make it very easy to implement the model of what
    applications and postings are (and what they do).

    Conceptually, a posting has a bundle of associated applications. Therefore, 
    we can think of a posting as an "application type," with its own distinct
    set of fields. With Entity API's "bundle of" functionality, an entity may
    define itself as a bundle (type) of another entity. This is not only easier
    to implement, but offers a quick and definitive means of telling which
    posting an application was submitted against. 
    
    More importantly, making postings bundles of applications means that these
    bundles may be made fieldable. This allows each posting (described as an
    entity in its own right) to have its own associated application form, using
    the same UI patterns as content types.
    
    Finally, because they are not coupled to the node system in any way, 
    implementing entity-specific behavior (such as automatically enforcing a
    posting's deadline when submitting an application) becomes much easier. All
    postings and applications would inherit the same basic behavior, without any
    other advanced configuration.
